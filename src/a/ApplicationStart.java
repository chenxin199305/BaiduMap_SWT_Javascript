package a;

import org.eclipse.swt.widgets.Display;

import view.BaiduMapShell;

public class ApplicationStart {

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			
			ApplicationStart window = new ApplicationStart();
			
			
			Display display = Display.getDefault();
			
			BaiduMapShell baiduMapShell = new BaiduMapShell(display);
			
			baiduMapShell.open();
			baiduMapShell.layout();
			
			while (!baiduMapShell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
