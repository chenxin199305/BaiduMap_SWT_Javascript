package view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

public class BaiduMapShell extends Shell {

	private BaiduMapComposite baiduMapBrowserComposite;

	private boolean BaiduMapIsOpenFlag = false;
	
	/**
	 * Create the shell.
	 * @param display
	 */
	public BaiduMapShell(Display display) {
		super(display, SWT.SHELL_TRIM);

		setText("康复项目——百度云地图显示程序");
		setSize(508, 400);
		
		createContents();
		initEventListener();
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		
		baiduMapBrowserComposite = new BaiduMapComposite(this, SWT.NONE);
		baiduMapBrowserComposite.setBounds(10, 10, 470, 346);
	}
	
	/**
	 *	初始化事件监听 
	 */
	private void initEventListener() {
		
		baiduMapBrowserComposite.getLoadBaiduMapHTMLAddressButton().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				
				chooseHTMLFile();
			}
		});
		
		baiduMapBrowserComposite.getStartStopButton().addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseDown(MouseEvent e) {
				
				if (BaiduMapIsOpenFlag == false) {
					
					if (baiduMapBrowserComposite.getBaiduMapHTMLAddressText().getText().getBytes().length < 1) {
						
						return;
					}
					
					baiduMapBrowserComposite.getBaiduMapBrowser().setJavascriptEnabled(true);
					baiduMapBrowserComposite.getBaiduMapBrowser().setUrl(baiduMapBrowserComposite.getBaiduMapHTMLAddressText().getText());
					
					BaiduMapIsOpenFlag = true;
					baiduMapBrowserComposite.getStartStopButton().setText(BaiduMapComposite.stopString);
				}
				else {
					
					baiduMapBrowserComposite.getBaiduMapBrowser().stop();
					
					BaiduMapIsOpenFlag = false;
					baiduMapBrowserComposite.getStartStopButton().setText(BaiduMapComposite.startString);
				}
			}
		});
	}
	
	private void chooseHTMLFile() {
		
		FileDialog fileDialog = new FileDialog(this, SWT.OPEN);
		fileDialog.setText("请选择百度地图 javascript_html 文件");
		String fileName = fileDialog.open();
		System.out.println("fileName = "+fileName);				// 获取的是绝对路径
		
		if (fileName != null) {
		
			baiduMapBrowserComposite.getBaiduMapHTMLAddressText().setText(fileName);
		}
		else {
			
			baiduMapBrowserComposite.getBaiduMapHTMLAddressText().setText("");
		}
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
