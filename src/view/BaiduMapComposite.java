package view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;


public class BaiduMapComposite extends Composite {
	
	public static String startString = "开启";
	public static String stopString = "关闭";
	
	private Browser baiduMapBrowser;
	private Text baiduMapHTMLAddressText;
	private Button loadBaiduMapHTMLAddressButton;
	private Button startStopButton;
	
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public BaiduMapComposite(Composite parent, int style) {
		super(parent, style);
		
		baiduMapBrowser = new Browser(this, SWT.NONE);
		baiduMapBrowser.setBounds(10, 10, 452, 283);
		
		baiduMapHTMLAddressText = new Text(this, SWT.BORDER);
		baiduMapHTMLAddressText.setToolTipText("baidu map html");
		baiduMapHTMLAddressText.setBounds(10, 299, 250, 39);
		
		loadBaiduMapHTMLAddressButton = new Button(this, SWT.NONE);
		loadBaiduMapHTMLAddressButton.setBounds(266, 299, 95, 29);
		loadBaiduMapHTMLAddressButton.setText("载入文件");
		
		startStopButton = new Button(this, SWT.NONE);
		startStopButton.setText(startString);
		startStopButton.setBounds(367, 299, 95, 29);
	}

	public Browser getBaiduMapBrowser() {
		return baiduMapBrowser;
	}

	public Text getBaiduMapHTMLAddressText() {
		return baiduMapHTMLAddressText;
	}
	
	public Button getLoadBaiduMapHTMLAddressButton() {
		return loadBaiduMapHTMLAddressButton;
	}

	public Button getStartStopButton() {
		return startStopButton;
	}

	public void setBaiduMapBrowser(Browser baiduMapBrowser) {
		this.baiduMapBrowser = baiduMapBrowser;
	}

	public void setBaiduMapHTMLAddressText(Text baiduMapHTMLAddressText) {
		this.baiduMapHTMLAddressText = baiduMapHTMLAddressText;
	}
	
	public void setLoadBaiduMapHTMLAddressButton(Button loadBaiduMapHTMLAddressButton) {
		this.loadBaiduMapHTMLAddressButton = loadBaiduMapHTMLAddressButton;
	}

	public void setStartStopButton(Button startStopButton) {
		this.startStopButton = startStopButton;
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
